#include <iostream>
using namespace std;

int main() {
    float bil1 = 0, bil2 = 0, hasil = 0;
    char pilih = ' ';
    
    cout<<"Masukkan Bilangan ke 1         : ";cin>>bil1;
    cout<<"Masukkan Operator (x, :, +, -) : ";cin>>pilih;
    cout<<"Masukkan Bilangan ke 2         : ";cin>>bil2;
    
    switch (pilih) {
        case 'x':
            hasil = bil1 * bil2;
            cout<<"Bilangan ke 1 x Bilangan ke 2 = "<<bil1<<" x "<<bil2<<" = "<<hasil;
            break;
        
        case ':':
            hasil = bil1 / bil2;
            cout<<"Bilangan ke 1 : Bilangan ke 2 = "<<bil1<<" : "<<bil2<<" = "<<hasil;
            break;
            
        case '+':
            hasil = bil1 + bil2;
            cout<<"Bilangan ke 1 + Bilangan ke 2 = "<<bil1<<" + "<<bil2<<" = "<<hasil;
            break;
            
        case '-':
            hasil = bil1 - bil2;
            cout<<"Bilangan ke 1 - Bilangan ke 2 = "<<bil1<<" - "<<bil2<<" = "<<hasil;
            break;
            
        default:
            cout<<"Operator yang anda masukkan salah";
            
    }

    return 0;
}
